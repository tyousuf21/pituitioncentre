package Authentication;
import java.util.List;
import Model.User;
import Model.Student;
import Model.Tuition;

public class Authentication {
    private static Authentication instance;
    private User current_user;

    private Authentication() {

    }
    
    

    public static Authentication get_instance() {
        if (instance == null) {
            synchronized (Authentication.class) {
                if (instance == null) {
                    instance = new Authentication();
                }
            }
        }
        return instance;
    }


    public void Logout() {
        current_user = null;
    }

    public User get_current_user() {
        return current_user;
    }

    public void set_current_user(Student currentUser) {
        this.current_user = currentUser;
    }

    public boolean Login(String email, String password) {
        List<User> users = Tuition.get_instance().get_users();
        for (User user : users) {
            if (user.get_email().equalsIgnoreCase(email) && user.get_password().equals(password)) {
                current_user = user;
                return true;
            }
        }


        return false;
    }
}
