package Model;

public abstract class User {
    public static final String ADMIN = "management";
    public static final String STUDENT = "student";
    private String email;
    private String password;
    private String type;


    public User(String email, String password, String type) {
        this.email = email;
        this.password = password;
        this.type = type;
    }

    public String get_type() {
        return type;
    }

    public void set_type(String type) {
        this.type = type;
    }

    public String get_email() {
        return email;
    }

    public void set_email(String email) {
        this.email = email;
    }

    public String get_password() {
        return password;
    }

    public void set_password(String password) {
        this.password = password;
    }
}