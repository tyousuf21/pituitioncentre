package Model;
import Authentication.Authentication;
import java.util.Scanner;

public class Console {
    public static void menu() {
        System.out.println("1. Login");
        System.out.println("2. Logout");
        System.out.println("3. Add Booking");
        System.out.println("4. Change Bookings");
        System.out.println("5. View Timetable");
        System.out.println("6. Get Books");
        System.out.println("7. Report of Lessons");
        System.out.println("8. Report of Highest Earning Lesson");
        System.out.println("9. Add Book");
        System.out.println("10. Cancel Booking");
        System.out.println("11. Give Review");
        System.out.println("12. Exit");
    }

    public static void lesson_names() {
        System.out.println("1. English");
        System.out.println("2. Math");
        System.out.println("3. Verbal Reasoning");
        System.out.println("4. Non-Verbal Reasoning");
    }

    public static void days() {
        System.out.println("1. SATURDAY");
        System.out.println("2. SUNDAY");
    }

    public static void time() {
        System.out.println("1. MORNING");
        System.out.println("2. AFTERNOON");
        System.out.println("3. EVENING");
    }

    public static void reviews() {
        System.out.println("1. Very Dissatisfied");
        System.out.println("2. Dissatisfied");
        System.out.println("3. OK");
        System.out.println("4. Satisfied");
        System.out.println("5. Very Satisfied");
    }

    public static void main(String[] args) {
        Tuition tution = Tuition.get_instance();
        int choice = 0;
        Scanner scanner = new Scanner(System.in);

        while (choice != 12) {
            menu();
            choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1:
                    login_menu();
                    break;
                case 2:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        Authentication.get_instance().Logout();
                    }
                    break;
                case 3:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        String lessonName, day, time;
                        lessonName = day = time = "not selected";
                        while (true) {
                            lesson_names();
                            System.out.print("Choose Lesson: ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > 4)
                                System.out.println("Wrong Input");
                            else {
                                switch (choice) {
                                    case 1:
                                        lessonName = "MATH";
                                        break;
                                    case 2:
                                        lessonName = "English";
                                        break;
                                    case 3:
                                        lessonName = "NON-Verbal Reasoning";
                                        break;
                                    case 4:
                                        lessonName = "Verbal Reasoning";
                                        break;
                                }
                                break;
                            }
                        }

                        while (true) {
                            days();
                            System.out.print("Choose Day: ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > 2)
                                System.out.println("Wrong Input");
                            else {
                                switch (choice) {
                                    case 1:
                                        day = "SATURDAY";
                                        break;
                                    case 2:
                                        day = "SUNDAY";
                                        break;
                                }
                                break;
                            }
                        }

                        while (true) {
                            time();
                            System.out.print("Choose Time: ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > 3)
                                System.out.println("Wrong Input");
                            else {
                                switch (choice) {
                                    case 1:
                                        time = "MORNING";
                                        break;
                                    case 2:
                                        time = "AFTERNOON";
                                        break;
                                    case 3:
                                        time = "EVENING";
                                        break;
                                }
                                break;
                            }
                        }
                        tution.create_booking(lessonName, day, time);
                    }
                    break;
                case 4:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        System.out.println(tution.get_booking_list());
                        while (true) {
                            System.out.print("Enter which booking to change (i.e. 1,2,...): ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > tution.get_my_bookings().size()) {
                                System.out.println("Wrong input");
                            } else {
                                break;
                            }
                        }
                        int bookingNumber = choice;
                        String time, day;
                        time = day = "";
                        System.out.println("Input New Time and Date");
                        while (true) {
                            days();
                            System.out.print("Choose Day: ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > 2)
                                System.out.println("Wrong Input");
                            else {
                                switch (choice) {
                                    case 1:
                                        day = "SATURDAY";
                                        break;
                                    case 2:
                                        day = "SUNDAY";
                                        break;
                                }
                                break;
                            }
                        }

                        while (true) {
                            time();
                            System.out.print("Choose Time: ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > 3)
                                System.out.println("Wrong Input");
                            else {
                                switch (choice) {
                                    case 1:
                                        time = "MORNING";
                                        break;
                                    case 2:
                                        time = "AFTERNOON";
                                        break;
                                    case 3:
                                        time = "EVENING";
                                        break;
                                }
                                break;
                            }
                        }
                        tution.get_my_bookings().get(bookingNumber - 1).set_day(day);
                        tution.get_my_bookings().get(bookingNumber - 1).set_day(time);
                    }
                    break;
                case 5:
                    break;
                case 6:
                    System.out.println(tution.get_books());
                    break;
                case 7:
                    System.out.println(tution.lesson_report());
                    break;
                case 8:
                    System.out.println(tution.lesson_high_income());
                    break;
                case 9:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        System.out.print("Enter Book Name: ");
                        String bookName = scanner.nextLine();
                        User currentUser = Authentication.get_instance().get_current_user();
                        if (currentUser.get_type().equals(User.ADMIN))
                            System.out.println("You are admin");
                        else {
                            Student student = (Student) currentUser;
                            student.add_book(bookName);
                        }
                    }
                    break;
                case 10:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        System.out.println(tution.get_booking_list());
                        while (true) {
                            System.out.print("Enter which booking to cancel (i.e. 1,2,...): ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > tution.get_my_bookings().size()) {
                                System.out.println("Wrong input");
                            } else {
                                break;
                            }
                        }
                        tution.cancel_booking(tution.get_my_bookings().get(choice - 1));
                    }
                    break;
                case 11:
                    if (!already_login(Authentication.get_instance().get_current_user())) {
                        System.out.println("Login First");
                    } else {
                        System.out.println(tution.get_booking_list());
                        while (true) {
                            System.out.print("Enter which Lesson to review (i.e. 1,2,...): ");
                            choice = Integer.parseInt(scanner.nextLine());
                            if (choice < 1 || choice > tution.get_my_bookings().size()) {
                                System.out.println("Wrong input");
                            } else {
                                break;
                            }
                        }
                        int review;
                        while (true) {
                            reviews();
                            System.out.print("Enter Review: ");
                            review = Integer.parseInt(scanner.nextLine());
                            if (review < 1 || review > 5) {
                                System.out.println("Wrong Input");
                            } else {
                                break;
                            }
                        }
                        tution.get_my_bookings().get(choice - 1).get_lesson().add_review(review);
                    }
                    break;
            }
            System.out.println("\n\n");
        }
    }

    private static void login_menu() {

        String email;
        System.out.print("Enter Email to login: ");
        Scanner scanner = new Scanner(System.in);
        email = scanner.nextLine();
        System.out.print("Enter Password to login: ");
        String password = new Scanner(System.in).nextLine();
        if (!Authentication.get_instance().Login(email, password)) {
            System.out.println("Login Failed");
        }


    }

    public static boolean already_login(User user) {
        return user != null;
    }
}

