package Model;
import java.util.List;
import java.util.ArrayList;

public class lesson {
    private static final int MAX_STUDENTS = 4;
    private String name;
    private double price;
    private int current_students;
    private List<Integer> reviews;

    public lesson(String name, double price) {
        this.name = name;
        this.price = price;
        this.current_students = 0;
        reviews = new ArrayList<>();
    }

    public String get_name() {
        return name;
    }

    public void set_name(String name) {
        this.name = name;
    }

    public double get_price() {
        return price;
    }

    public void set_price(double price) {
        this.price = price;
    }

    public int get_current_students() {
        return current_students;
    }

    public void set_current_students(int current_students) {
        this.current_students = current_students;
    }

    public void add_review(int review) {
        reviews.add(review);
    }

    public double get_average_review() {
        double sum = 0;
        for (int x : reviews)
            sum += x;
        return sum / reviews.size();
    }

    public void add_student() {
        if (current_students < MAX_STUDENTS) {
            current_students++;
        }
    }
}
