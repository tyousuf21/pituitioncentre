package Model;


public class Booking {
    private Student student;
    private lesson lesson;
    private String day, time;
    private boolean status;

    public Booking(Student student, lesson lesson, boolean status) {
        this.student = student;
        this.lesson = lesson;
        this.status = status;
        this.lesson.add_student();
    }

    public Booking(Student student, lesson lesson) {
        this.student = student;
        this.lesson = lesson;
        this.lesson.add_student();
        this.status = false;
    }

    public Booking(Student student, lesson lesson, String day, String time) {
        this.student = student;
        this.lesson = lesson;
        this.day = day;
        this.time = time;
        this.lesson.add_student();
        this.status = false;
    }

    public String get_day() {
        return day;
    }

    public void set_day(String day) {
        this.day = day;
    }

    public String get_time() {
        return time;
    }

    public void set_time(String time) {
        this.time = time;
    }

    public Student get_student() {
        return student;
    }

    public void set_student(Student student) {
        this.student = student;
    }

    public lesson get_lesson() {
        return lesson;
    }

    public void set_lesson(lesson lesson) {
        this.lesson = lesson;
    }

    public boolean has_attended() {
        return status;
    }

    public void set_status(boolean status) {
        this.status = status;
    }

}