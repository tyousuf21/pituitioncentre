package Model;


public class Management extends User {

    public Management(String email, String password) {
        super(email, password, User.ADMIN);
    }
}
