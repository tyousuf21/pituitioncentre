package Model;
import java.util.ArrayList;
import java.util.List;

public class Student extends User {
    private String name, gender, dob, address, emergency_contact;
    private List<String> books;

    public Student(String name, String gender, String dob, String address, String emergency_contact, String email, String password) {
        super(email, password, User.STUDENT);
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.emergency_contact = emergency_contact;
        books = new ArrayList<>();
    }

    public Student(String name, String gender, String dob, String address, String emergency_contact, List<String> books, String email, String password) {
        super(email, password, User.STUDENT);
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.emergency_contact = emergency_contact;
        this.books = books;
    }



    public String get_name() {
        return name;
    }

    public void set_name(String name) {
        this.name = name;
    }

    public String get_gender() {
        return gender;
    }

    public void set_gender(String gender) {
        this.gender = gender;
    }

    public String get_dob() {
        return dob;
    }

    public void set_dob(String dob) {
        this.dob = dob;
    }

    public String get_address() {
        return address;
    }

    public void set_address(String address) {
        this.address = address;
    }

    public String get_emergency_contact() {
        return emergency_contact;
    }

    public void set_emergency_contact(String emergency_contact) {
        this.emergency_contact = emergency_contact;
    }

    public List<String> get_books() {
        return books;
    }

    public void set_books(List<String> books) {
        this.books = books;
    }

    public void add_book(String name) {
        this.books.add(name);
    }


}


