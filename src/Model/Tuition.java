package Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Formatter;
import Authentication.Authentication;

public class Tuition {
    private List<Student> students;
    private List<Management> admins;
    private List<lesson> lessons;
    private List<Booking> bookings;
    private static Tuition instance;

    public static Tuition get_instance() {
        if (instance == null) {
            synchronized (Authentication.class) {
                if (instance == null) {
                    instance = new Tuition();
                }
            }
        }
        return instance;
    }
    public List<Student> get_students() {
        return students;
    }
    public void set_students(List<Student> students) {
        this.students = students;
    }

    public List<lesson> get_lessons() {
        return lessons;
    }

    public void set_lessons(List<lesson> lessons) {
        this.lessons = lessons;
    }

    public List<Booking> get_bookings() {
        return bookings;
    }

    public void set_bookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    private Tuition() {
        students = new ArrayList<>();
        lessons = new ArrayList<>();
        bookings = new ArrayList<>();
        admins = new ArrayList<>();
        admins.add(new Management("management@gmail.com", "admin"));
        lessons.add(new lesson("English", 9000));
        lessons.add(new lesson("Math", 14000));
        lessons.add(new lesson("Verbal Reasoning", 5000));
        lessons.add(new lesson("Non-verbal Reasoning", 8000));
        
        students.add(new Student("Nausheen", "Female", "08/9/1996", "Defence PH8", "+923002330809", "student1@gmail.com", "PASSWORD"));
        students.add(new Student("Daniyal ", "Male", "03/1/1997", "Defence PH2", "+923322103090", "student2@gmail.com", "PASSWORD"));
        students.add(new Student("Taha", "Male", "08/02/2000", "Gulshan Block9", "+923352300707", "student3@gmail.com", "PASSWORD"));
        students.add(new Student("Yusra", "Female", "14/01/1998", "Bahadarabad", "+923322821245", "student4@gmail.com", "PASSWORD"));
        students.add(new Student("Sumrah", "Female", "30/09/1998", "Bahadarabad", "+923012624380", "student5@gmail.com", "PASSWORD"));
        
        students.add(new Student("Filza", "Female", "05/02/1999", "Chundigar road", "+0324814237", "student6@gmail.com", "PASSWORD"));
        students.add(new Student("Laiba", "Female", "27/06/1999", "Shehbaz", "+92309134977", "student7@gmail.com", "PASSWORD"));
        students.add(new Student("Aaly", "Male", "24/08/1992", "Address 8", "+9232450700", "student8@gmail.com", "PASSWORD"));
        students.add(new Student("Haseeb", "Male", "17/08/2000", "Kutchi Memon Society", "+923357333103", "student9@gmail.com", "PASSWORD"));
        students.add(new Student("Zafir", "Male", "03/11/1997", "Defence PH1", "+92332550097", "student10@gmail.com", "PASSWORD"));
        
        students.add(new Student("James Faulkner", "Male", "25/04/19997", "Korangi", "+1111111", "student11@gmail.com", "PASSWORD"));
        students.add(new Student("Tooba", "Female", "1/01/1998", "Tariq road", "+923353223327", "student12@gmail.com", "PASSWORD"));
        students.add(new Student("Leone", "Male", "24/08/1999", "Gulistan e Johar", "+2222222", "student13@gmail.com", "PASSWORD"));
        students.add(new Student("Mathew", "Male", "15/02/1999", "Namzimabad", "+3333333", "student14@gmail.com", "PASSWORD"));
        students.add(new Student("Aaliyan", "Male", "17/08/2000", "Gulshan Block10", "+923357333103", "student15@gmail.com", "PASSWORD"));
        
        bookings.add(new Booking(students.get(0), lessons.get(0), "SUNDAY", "EVENING"));
        bookings.add(new Booking(students.get(0), lessons.get(1), "SUNDAY", "MORNING"));
        bookings.add(new Booking(students.get(1), lessons.get(0), "SUNDAY", "EVENING"));
        bookings.add(new Booking(students.get(1), lessons.get(1), "SUNDAY", "AFTERNOON"));
        bookings.add(new Booking(students.get(1), lessons.get(2), "SATURDAY", "AFTERNOON"));
        bookings.add(new Booking(students.get(1), lessons.get(3), "SUNDAY", "AFTERNOON"));
        bookings.add(new Booking(students.get(5), lessons.get(1), "SATURDAY", "AFTERNOON"));
        bookings.add(new Booking(students.get(4), lessons.get(3), "SATURDAY", "MORNING"));
        bookings.add(new Booking(students.get(6), lessons.get(3), "SATURDAY", "EVENING"));
        bookings.add(new Booking(students.get(8), lessons.get(2), "SATURDAY", "MORNING"));
        bookings.add(new Booking(students.get(8), lessons.get(1), "SUNDAY", "MORNING"));
        bookings.add(new Booking(students.get(9), lessons.get(2), "SATURDAY", "EVENING"));
        bookings.add(new Booking(students.get(12), lessons.get(0), "SUNDAY", "EVENING"));
        bookings.add(new Booking(students.get(13), lessons.get(2), "SATURDAY", "AFTERNOON"));
        bookings.add(new Booking(students.get(3), lessons.get(0), "SATURDAY", "EVENING"));
        bookings.add(new Booking(students.get(3), lessons.get(3), "SATURDAY", "AFTERNOON"));
        lessons.get(0).add_review(5);
        lessons.get(0).add_review(5);
        lessons.get(0).add_review(2);
        lessons.get(0).add_review(5);
        lessons.get(0).add_review(5);
        lessons.get(0).add_review(5);
        lessons.get(0).add_review(5);
        lessons.get(1).add_review(2);
        lessons.get(1).add_review(2);
        lessons.get(1).add_review(2);
        lessons.get(1).add_review(2);
        lessons.get(1).add_review(1);
        lessons.get(2).add_review(4);
        lessons.get(2).add_review(5);
        lessons.get(2).add_review(2);
        lessons.get(2).add_review(1);
        lessons.get(2).add_review(1);
        lessons.get(2).add_review(1);
        lessons.get(2).add_review(1);
        lessons.get(3).add_review(1);
        lessons.get(3).add_review(1);
    }

    public String lesson_report() {
        StringBuilder to_return = new StringBuilder("");
        for (lesson lesson : lessons) {
            to_return.append("Lesson Name: ").append(lesson.get_name()).append("\tCurrent Students: ").append(lesson.get_current_students()).append("\tAverage Review: ").append(lesson.get_average_review()).append("\n");
        }
        return to_return.toString();

    }

    public String lesson_high_income() {
        double max = 0;
        String lesson_name = "";
        for (lesson lesson : lessons) {
            double local_max = lesson.get_price() * lesson.get_current_students();
            if (local_max > max) {
                max = local_max;
                lesson_name = lesson.get_name();
            }
        }
        return "Highest Income generated by " + lesson_name + ", and the income is " + max;
    }

    public String get_books() {
        StringBuilder to_return = new StringBuilder();
        for (Student student : students) {
            for (String s : student.get_books()) {
                to_return.append("Book Name: ").append(s).append("\n");
            }
        }
        return to_return.toString();
    }

    public void create_booking(String lessonName, String day, String time) {
        lesson lesson = null;
        User current_user = Authentication.get_instance().get_current_user();
        if (current_user.get_type().equals(User.ADMIN))
            throw new RuntimeException("You are an admin");
        Student student = (Student) current_user;
        for (lesson l : lessons) {
            if (l.get_name().equalsIgnoreCase(lessonName)) {
                lesson = l;
                break;
            }
        }
        bookings.add(new Booking(student, lesson, day, time));
    }

    public String get_booking_list() {
        int i = 0;
        User current_user = Authentication.get_instance().get_current_user();
        if (current_user.get_type().equals(User.ADMIN))
            throw new RuntimeException("You are an admin");
        Student student = (Student) current_user;
        StringBuilder to_return = new StringBuilder();
        for (Booking booking : bookings) {
            if (booking.get_student().get_name().equalsIgnoreCase(student.get_name())) {
                to_return.append(++i).append(". Lesson Name: ").append(booking.get_lesson().get_name()).append(" Day/Time: ").append(booking.get_day()).append("/").append(booking.get_time()).append(" \n");
            }
        }
        return to_return.toString();
    }
    public List<Booking> get_current_bookings(){
        List<Booking> current_bookings = new ArrayList<>();
        User current_user = Authentication.get_instance().get_current_user();
        Student student = (Student) current_user;
        for (Booking booking : bookings) {
            if (booking.get_student().get_name().equalsIgnoreCase(student.get_name())) {
              current_bookings.add(booking);
            }
        }

        return current_bookings;
    }

    public List<Booking> get_my_bookings() {
        User current_user = Authentication.get_instance().get_current_user();
        if (current_user.get_type().equals(User.ADMIN))
            throw new RuntimeException("You are an admin");
        Student student = (Student) current_user;
        List<Booking> bookings = new ArrayList<>();
        for (Booking booking : this.bookings) {
            if (booking.get_student().get_name().equalsIgnoreCase(student.get_name())) {
                bookings.add(booking);
            }
        }
        return bookings;
    }

    public void cancel_booking(Booking booking) {
        bookings.remove(booking);
    }

    public List<User> get_users() {
        List<User> users = new ArrayList<>();
        users.addAll(students);
        users.addAll(admins);
        return users;
    }

    public List<String> get_days() {
        List<String> days = new ArrayList<>();
        days.add("SATURDAY");
        days.add("SUNDAY");
        return days;
    }

    public List<String> get_timings() {
        List<String> timings = new ArrayList<>();
        timings.add("MORNING");
        timings.add("AFTERNOON");
        timings.add("EVENING");
        return timings;
    }
}
