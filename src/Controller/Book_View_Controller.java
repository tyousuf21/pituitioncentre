package Controller;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import Authentication.Authentication;
import Model.lesson;
import Model.Student;
import Model.Tuition;

public class Book_View_Controller extends javax.swing.JFrame {

    public Book_View_Controller() {
        init_components();
        init_values();
    }

    @SuppressWarnings("unchecked")
    private void init_components() {

        backBtn = new javax.swing.JButton();
        subjectsListFrame = new javax.swing.JScrollPane();
        booksPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        addBooksBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        backBtn.setText("<");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        booksPanel.setLayout(new javax.swing.BoxLayout(booksPanel, javax.swing.BoxLayout.Y_AXIS));
        subjectsListFrame.setViewportView(booksPanel);

        jLabel1.setText("Enrolled Books");

        addBooksBtn.setText("+");
        addBooksBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBooksBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backBtn)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(subjectsListFrame, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 297, Short.MAX_VALUE)
                        .addComponent(addBooksBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backBtn)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(addBooksBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(subjectsListFrame, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Student_Portal_Controller controller = new Student_Portal_Controller();
        controller.setVisible(true);
        this.setVisible(false);
    }

    private void addBooksBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Book_Controller controller = new Book_Controller();
        controller.setVisible(true);
        this.setVisible(false);

    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Book_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Book_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Book_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Book_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Book_View_Controller().setVisible(true);
            }
        });
    }

    private void init_values() {
        subjectsListFrame.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        subjectsListFrame.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        Student std = (Student) Authentication.get_instance().get_current_user();
        for (String s : std.get_books()) {
            JPanel mPanel = new JPanel(new GridLayout(1, 1));

            JLabel nameLabel = new JLabel("Book Name: " + s);
            nameLabel.setPreferredSize(new Dimension(100, 40));
            mPanel.add(nameLabel);
            mPanel.setMaximumSize(new Dimension(500, 40));
           
            booksPanel.add(mPanel);
        }
    }
    
    private javax.swing.JButton addBooksBtn;
    private javax.swing.JButton backBtn;
    private javax.swing.JPanel booksPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane subjectsListFrame;
}