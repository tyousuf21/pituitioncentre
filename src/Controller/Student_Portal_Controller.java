package Controller;
import Authentication.Authentication;
import Model.Student;

public class Student_Portal_Controller extends javax.swing.JFrame {

    public Student_Portal_Controller() {
        init_components();
        load_values();
    }


    @SuppressWarnings("unchecked")
    private void init_components() {

        jRadioButton1 = new javax.swing.JRadioButton();
        logoutBtn = new javax.swing.JButton();
        editDetailsBtn = new javax.swing.JButton();
        studentNameLbl = new javax.swing.JLabel();
        emailLbl = new javax.swing.JLabel();
        contactLbl = new javax.swing.JLabel();
        dobLbl = new javax.swing.JLabel();
        viewSubjectsBtn = new javax.swing.JButton();
        viewTimeTable = new javax.swing.JButton();
        viewBookBtn = new javax.swing.JButton();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        logoutBtn.setText("Logout");
        logoutBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutBtnActionPerformed(evt);
            }
        });

        editDetailsBtn.setText("Edit Student details");
        editDetailsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editDetailsBtnActionPerformed(evt);
            }
        });

        studentNameLbl.setText("Student Name");

        emailLbl.setText("Email");

        contactLbl.setText("Contact");

        dobLbl.setText("dob");

        viewSubjectsBtn.setText("View Subjects");
        viewSubjectsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewSubjectsBtnActionPerformed(evt);
            }
        });

        viewTimeTable.setText("View Time Table");
        viewTimeTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewTimeTableActionPerformed(evt);
            }
        });

        viewBookBtn.setText("View Books");
        viewBookBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewBookBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(emailLbl)
                            .addComponent(contactLbl))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(studentNameLbl)
                        .addGap(103, 103, 103)
                        .addComponent(dobLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                        .addComponent(logoutBtn)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editDetailsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(viewSubjectsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(viewTimeTable, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(viewBookBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logoutBtn)
                    .addComponent(studentNameLbl)
                    .addComponent(dobLbl))
                .addGap(1, 1, 1)
                .addComponent(emailLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contactLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addComponent(editDetailsBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewSubjectsBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewTimeTable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewBookBtn)
                .addGap(51, 51, 51))
        );

        pack();
    }

    private void logoutBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Home_Controller controller = new Home_Controller();
        Authentication.get_instance().Logout();
        controller.setVisible(true);
        this.setVisible(false);
    }

    private void editDetailsBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Student_Edit_Detail_Controller controller = new Student_Edit_Detail_Controller();
        controller.setVisible(true);
        this.setVisible(false);

    }

    private void viewSubjectsBtnActionPerformed(java.awt.event.ActionEvent evt) {


        Subjects_View_Controller controller = new Subjects_View_Controller();
        controller.setVisible(true);
        this.setVisible(false);


    }

    private void viewTimeTableActionPerformed(java.awt.event.ActionEvent evt) {


    }

    private void viewBookBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Book_View_Controller bvc = new Book_View_Controller();
        this.setVisible(false);
        bvc.setVisible(true);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Student_Portal_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Student_Portal_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Student_Portal_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Student_Portal_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Student_Portal_Controller().setVisible(true);
            }
        });
    }

    private void load_values() {
        Student student = (Student) Authentication.get_instance().get_current_user();
        studentNameLbl.setText(student.get_name());
        emailLbl.setText(student.get_email());
        contactLbl.setText(student.get_emergency_contact());
        dobLbl.setText(student.get_dob());
    }

    private javax.swing.JLabel contactLbl;
    private javax.swing.JLabel dobLbl;
    private javax.swing.JButton editDetailsBtn;
    private javax.swing.JLabel emailLbl;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JButton logoutBtn;
    private javax.swing.JLabel studentNameLbl;
    private javax.swing.JButton viewBookBtn;
    private javax.swing.JButton viewSubjectsBtn;
    private javax.swing.JButton viewTimeTable;
}



