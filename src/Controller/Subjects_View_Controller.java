package Controller;
import Model.Booking;
import Model.lesson;
import Model.Tuition;
import javax.swing.*;
import java.awt.*;

public class Subjects_View_Controller extends javax.swing.JFrame {

    public Subjects_View_Controller() {
        init_components();
        init_values();
    }


    @SuppressWarnings("unchecked")
    private void init_components() {

        backBtn = new javax.swing.JButton();
        subjectsListFrame = new javax.swing.JScrollPane();
        subjectsPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        addSubjectsBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        backBtn.setText("<");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        subjectsPanel.setLayout(new javax.swing.BoxLayout(subjectsPanel, javax.swing.BoxLayout.Y_AXIS));
        subjectsListFrame.setViewportView(subjectsPanel);

        jLabel1.setText("Enrolled Subjects");

        addSubjectsBtn.setText("+");
        addSubjectsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSubjectsBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backBtn)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(subjectsListFrame, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 357, Short.MAX_VALUE)
                        .addComponent(addSubjectsBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backBtn)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(addSubjectsBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(subjectsListFrame, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Student_Portal_Controller controller = new Student_Portal_Controller();
        controller.setVisible(true);
        this.setVisible(false);
    }

    private void reload() {
        Subjects_View_Controller controller = new Subjects_View_Controller();
        controller.setVisible(true);
        this.setVisible(false);
    }


    private void addSubjectsBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Booking_Controller controller = new Booking_Controller();
        controller.setVisible(true);
        this.setVisible(false);


    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Subjects_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Subjects_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Subjects_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Subjects_View_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Subjects_View_Controller().setVisible(true);
            }
        });
    }

    private void init_values() {
        subjectsListFrame.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        subjectsListFrame.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);


        for (Booking booking : Tuition.get_instance().get_current_bookings()) {
            JPanel mPanel = new JPanel(new GridLayout(1, 3));
            lesson lesson = booking.get_lesson();
            String day = booking.get_day();
            String time = booking.get_time();
            JLabel nameLabel = new JLabel("Subject: " + lesson.get_name() + " Time: " + time + " " + day);
            nameLabel.setPreferredSize(new Dimension(100, 40));
            mPanel.add(nameLabel);
            mPanel.setMaximumSize(new Dimension(500, 40));
            if (!booking.has_attended()) {
                JButton attendLec = new JButton("Attend");
                attendLec.setMaximumSize(new Dimension(60, 40));
                attendLec.addActionListener(e -> {
                    booking.set_status(true);
                    JOptionPane.showConfirmDialog(this,
                            "Lecture status changed", "Attended", JOptionPane.DEFAULT_OPTION);
                    while (true) {
                        try {
                            String review = JOptionPane.showInputDialog(this, "Enter Review(1-5): ");
                            booking.get_lesson().add_review(Integer.parseInt(review));
                            break;
                        } catch (Exception exception) {
                        }
                    }
                    reload();
                });
                mPanel.add(attendLec);
            }
            JButton deleteBtn = new JButton("Cancel");
            deleteBtn.addActionListener(e -> {
                Tuition.get_instance().cancel_booking(booking);
                reload();
            });
            deleteBtn.setMaximumSize(new Dimension(60, 40));
            mPanel.add(deleteBtn);


            subjectsPanel.add(mPanel);
        }
    }


    private javax.swing.JButton addSubjectsBtn;
    private javax.swing.JButton backBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane subjectsListFrame;
    private javax.swing.JPanel subjectsPanel;
}
