package Controller;
import Model.lesson;
import Model.Tuition;
import javax.swing.*;
import java.util.List;

public class Booking_Controller extends javax.swing.JFrame {

    public Booking_Controller() {
        init_components();
        init_Values();
    }

    private void init_Values() {
        Tuition tuition = Tuition.get_instance();
        List<lesson> lessons = tuition.get_lessons();
        List<String> days = tuition.get_days();
        List<String> timing = tuition.get_timings();

        for (lesson lesson: lessons){
            subjectField.addItem(lesson.get_name());
        }
        for (String day: days){
            dateField.addItem(day);
        }
        for (String time: timing){
            timeField.addItem(time);
        }
    }

    @SuppressWarnings("unchecked")
    private void init_components() {

        backBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        Subject = new javax.swing.JLabel();
        subjectField = new javax.swing.JComboBox<>();
        DateField = new javax.swing.JLabel();
        dateField = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        timeField = new javax.swing.JComboBox<>();
        add = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        backBtn.setText("<");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        jLabel1.setText("Add Booking");
        Subject.setText("Subject");
        DateField.setText("Day");
        jLabel2.setText("Time");
        add.setText("Add");
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Subject)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel2)
                                .addComponent(DateField)))
                        .addGap(103, 103, 103)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(add)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(subjectField, 0, 93, Short.MAX_VALUE)
                                .addComponent(dateField, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(timeField, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(195, 195, 195)
                        .addComponent(jLabel1)))
                .addContainerGap(179, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backBtn)
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Subject)
                    .addComponent(subjectField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DateField)
                    .addComponent(dateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(timeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(add)
                .addContainerGap(205, Short.MAX_VALUE))
        );

        pack();
    }

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Student_Portal_Controller controller = new Student_Portal_Controller();
        controller.setVisible(true);
        this.setVisible(false);

    }

    private void addActionPerformed(java.awt.event.ActionEvent evt) {
        Tuition tuition = Tuition.get_instance();
        tuition.create_booking((String)subjectField.getSelectedItem(),(String) timeField.getSelectedItem(),(String) dateField.getSelectedItem());
        JOptionPane.showConfirmDialog(this,
                "Booking has been Added successfully", "Booking added!", JOptionPane.DEFAULT_OPTION);
        backBtnActionPerformed(null);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Booking_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Booking_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Booking_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Booking_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Booking_Controller().setVisible(true);
            }
        });
    }

    private javax.swing.JLabel DateField;
    private javax.swing.JLabel Subject;
    private javax.swing.JButton add;
    private javax.swing.JButton backBtn;
    private javax.swing.JComboBox<String> dateField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JComboBox<String> subjectField;
    private javax.swing.JComboBox<String> timeField;
}
