package Controller;
import Authentication.Authentication;
import Model.Tuition;

public class Admin_Controller extends javax.swing.JFrame {

    public Admin_Controller() {
        init_components();
    }

    @SuppressWarnings("unchecked")
    private void init_components() {

        logoutBtn1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        reportTxtArea = new javax.swing.JTextArea();
        genReportBtn = new javax.swing.JButton();
        genRevReportBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        logoutBtn1.setText("Logout");
        logoutBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutBtn1ActionPerformed(evt);
            }
        });

        reportTxtArea.setColumns(20);
        reportTxtArea.setRows(5);
        jScrollPane1.setViewportView(reportTxtArea);

        genReportBtn.setText("Generate Report");
        genReportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genReportBtnActionPerformed(evt);
            }
        });

        genRevReportBtn.setText("Generate Revenue Report");
        genRevReportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genRevReportBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(genReportBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(genRevReportBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 100, Short.MAX_VALUE)
                        .addComponent(logoutBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(genReportBtn)
                    .addComponent(genRevReportBtn)
                    .addComponent(logoutBtn1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }

    private void LogoutBtn1ActionPerformed(java.awt.event.ActionEvent evt) {

        Home_Controller controller = new Home_Controller();
        Authentication.get_instance().Logout();
        controller.setVisible(true);
        this.setVisible(false);
    }

    private void genReportBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Tuition t = Tuition.get_instance();
        reportTxtArea.setText(t.lesson_report());
    }

    private void genRevReportBtnActionPerformed(java.awt.event.ActionEvent evt) {
        Tuition t = Tuition.get_instance();
        reportTxtArea.setText(t.lesson_high_income());
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_Controller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Admin_Controller().setVisible(true);
            }
        });
    }

    private javax.swing.JButton genReportBtn;
    private javax.swing.JButton genRevReportBtn;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton logoutBtn1;
    private javax.swing.JTextArea reportTxtArea;
}
